import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Image from '../Image';
import Expand from '../Expand';
import './Gallery.scss';
import cloneDeep from 'lodash/cloneDeep';

class Gallery extends React.Component {
  static propTypes = {
    tag: PropTypes.string,
    onExpand: PropTypes.func,
    idx: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.state = {
      images: [],
      galleryWidth: this.getGalleryWidth(),
      expandIdx: -1,
      scrollY: 0,
      page: 1,
      windowHeight: window.innerHeight
    };
  }

  getGalleryWidth(){
    try {
      return document.body.clientWidth;
    } catch (e) {
      return 1000;
    }
  }

  getImages(tag, checker) {
    const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad&tags=${tag}&tag_mode=any&per_page=100&page=${this.state.page}&format=json&safe_search=1&nojsoncallback=1`;
    const baseUrl = 'https://api.flickr.com/';
    axios({
      url: getImagesUrl,
      baseURL: baseUrl,
      method: 'GET'
    })
      .then(res => res.data)
      .then(res => {
        if (
          res &&
          res.photos &&
          res.photos.photo &&
          res.photos.photo.length > 0
        ) {
          this.gettingImages = false;
          if(checker){
            this.setState({images: res.photos.photo});
          }
          else{
            this.setState({images: this.state.images.concat(res.photos.photo)});
          }
        }
      });
  }

  componentDidMount() {
    window.addEventListener('scroll', this.getMoreImages);
    //the second parameter is because i have to check if i have to add to the image array or to change the image array
    this.getImages(this.props.tag, true);
    this.setState({
      galleryWidth: document.body.clientWidth
    });
  }

  componentWillReceiveProps(props) {
    if(!props.boolFavorite){
      //because if i will change the tag when i am in the favorites i have to remove the favorite images
      this.getImages(props.tag, true);
    }
    else{
      this.setState({images: props.favoriteImages});
    }
  }

  handleClone = (dto, idx) => {
    let cpyimg = cloneDeep(dto);
    this.setState(this.state.images.splice(idx,0,cpyimg));
  }

  handleExpand = (expandIdx) => {
    this.setState({
      expandIdx
    });
  }

  handlefavorite = (url) => {
    if(localStorage.getItem(url)){
      localStorage.removeItem(url);
      let sum_from_tag = localStorage.getItem(this.props.tag);
      sum_from_tag --;
      localStorage.setItem(this.props.tag, sum_from_tag);
      alert('you removed the image from the favorite');
      if(this.props.boolFavorite){
        setTimeout(this.props.viewFavorite(), 50);
      }
    }
    else{
      localStorage.setItem(url, url);
      let sum_from_tag = localStorage.getItem(this.props.tag);
      sum_from_tag ++;
      localStorage.setItem(this.props.tag, sum_from_tag);
      alert('you added the image to the favorite');
    }
  }

  getMoreImages = () => {
    //Because I want the function to work only if there are no favorites
    if(!this.props.boolFavorite){
      this.setState({
        scrollY: window.scrollY,
        windowHeight: window.innerHeight
      });

      const scrollRemainder = document.body.clientHeight - scrollY - window.innerHeight;

      if (scrollRemainder < 500 && !this.gettingImages){
        this.gettingImages = true;
        this.setState({
          page: this.state.page + 1
        }, () => {
          this.getImages(this.props.tag, false);
        });
      }
    }
  }

  handleDrag = (index) => {
    this.state.drag = index;
  }

  handleDrop = (dto, idx) => {
    let {images} = this.state;
    images.splice(idx,1,);
    images.splice(this.state.drag,0,dto);
    this.setState({
      images
    });
  }

  render() {
    return (
      <div className="gallery-root">
        {this.state.images.map((dto, idx) => {
          return <Image key={'image-' + dto.id + idx}
                        dto={dto}
                        galleryWidth={this.state.galleryWidth}
                        idx={idx}
                        onClone={() => this.handleClone(dto, idx)}
                        onExpand={this.handleExpand}
                        onFavorite={this.handlefavorite}
                        handleDrag={() => this.handleDrag(idx)}
                        handleDrop={() => this.handleDrop(dto, idx)}/>;
        })}
        <Expand images={this.state.images} expandIdx={this.state.expandIdx} onClose={() => this.handleExpand(-1)}/>
      </div>
    );
  }
}

export default Gallery;
