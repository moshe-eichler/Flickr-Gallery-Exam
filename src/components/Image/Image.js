import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import './Image.scss';

class Image extends React.Component {
  static propTypes = {
    dto: PropTypes.object,
    galleryWidth: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.calcImageSize = this.calcImageSize.bind(this);
    this.state = {
      size: 200,
      flip: 'no-flip'
    }
  }

  calcImageSize() {
    const {galleryWidth} = this.props;
    const targetSize = 200;
    const imagesPerRow = Math.round(galleryWidth / targetSize);
    const size = (galleryWidth / imagesPerRow);
    this.setState({
      size
    });
  }

  componentDidMount() {
    this.calcImageSize();
  }

  urlFromDto(dto) {
    if(dto.farm){
      return `https://farm${dto.farm}.staticflickr.com/${dto.server}/${dto.id}_${dto.secret}.jpg`;
    }
    else{
      return dto.url;
    }
  }

  handleFlip = () => {
    if(this.state.flip == 'no-flip'){
      this.setState({flip: 'flip'});
    }
    else{
      this.setState({flip: 'no-flip'});
    }
  }

  render() {
    return (
      <div
        draggable={true}
        onDragLeave={() => this.props.handleDrag(this.props.dto.idx)}
        onDragEnd={() => this.props.handleDrop(this.props.dto)}
        className={`image-root ${this.state.flip}`}
        style={{
          backgroundImage: `url(${this.urlFromDto(this.props.dto)})`,
          width: this.state.size + 'px',
          height: this.state.size + 'px'
        }}
        >
        <div className={this.state.flip}>
          <FontAwesome className="image-icon" name="arrows-alt-h" title="flip" onClick={this.handleFlip}/>
          <FontAwesome className="image-icon" name="clone" title="clone" onClick={() => this.props.onClone(this.props.dto)}/>
          <FontAwesome className="image-icon" name="expand" title="expand" onClick={() => this.props.onExpand(this.props.idx)}/>
          <FontAwesome className="image-icon" name="heart" title="favorite" onClick={() => this.props.onFavorite(this.urlFromDto(this.props.dto))}/>
        </div>
      </div>
    );
  }
}

export default Image;
