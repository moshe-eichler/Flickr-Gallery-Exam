import React from 'react';
import './App.scss';
import Gallery from '../Gallery';
import FontAwesome from 'react-fontawesome';


class App extends React.Component {
  static propTypes = {
  };

  constructor() {
    super();
    this.state = {
      tag: 'car',
      boolFavorite: false,
      favoriteImages: []
    }
  }

  viewFavorites = () =>{
    this.state.favoriteImages = [];
    for (let index = 0; index < localStorage.length; index++) {
      var element = localStorage.key(index);
      //Because I have to sort out the tag's
      var check = element.startsWith('https://');
      if(check){
        let obj = {indx: index, url: element};
        this.state.favoriteImages.push(obj);
      }
    }
    this.setState({favoriteImages: this.state.favoriteImages, boolFavorite: true});
  }

  allImages = () => {
    this.setState({boolFavorite: false});
  }

  theMostFavorites = () => {
    let myArr = [];
    for (let index = 0; index < localStorage.length; index++) {
      var element = localStorage.key(index);
      //Because I have to sort out the url's
      var check = element.startsWith('https');
      if(check == false){
        let sum = localStorage.getItem(element);
        let obj = {tag: element, num: sum};
        myArr.push(obj);
      }
    }
    var maximum = Math.max.apply(Math, myArr.map(function(o) { return o.num; }));
    for (let index = 0; index < myArr.length; index++) {
      var check = myArr[index].num;
      if(check == maximum){
        this.setState({
          tag: myArr[index].tag, boolFavorite: false
        });
      }
    }
  }

  render() {
    return (
      <div className="app-root">
        <div className="app-header">
          <ul className="list">
            <li><FontAwesome className="button-icon" name="fas fa-star" title="Favorite Images" onClick={this.viewFavorites}/></li>
            <li><FontAwesome className="button-icon" name="fas fa-images" title="All  Images" onClick={this.allImages}/></li>
            <li><FontAwesome className="button-icon" name="fas fa-bookmark" title="Favorites tag" onClick={this.theMostFavorites}/></li>
          </ul>
          <h2>Flickr Gallery</h2>
          <input className="app-input" onChange={event => this.setState({tag: event.target.value,boolFavorite: false})} value={this.state.tag}/>
        </div>
        <Gallery tag={this.state.tag} viewFavorite={this.viewFavorites} favoriteImages={this.state.favoriteImages} boolFavorite={this.state.boolFavorite}/>
      </div>
    );
  }
}

export default App;

<i class="fas fa-bookmark"></i>